<?php
require_once __DIR__ . '/interfaces/CacheInterface.php';

class Cache implements CacheInterface
{
    private $name;
    private $dir;
    private $extension;
    private $path;
    private $cache;

    /**
     * Cache constructor.
     * @param string $name
     * @param string $dir
     * @param string $extension
     * @throws Exception
     */
    public function __construct(string $name = "LoreumIpsum", string $dir = "tmp/", string $extension = ".cache")
    {
        $this->name = $name;
        $this->dir = $dir;
        $this->extension = $extension;
        $this->path = $this->cachePath();
        try {
            $this->checkCacheDir();
        } catch (Exception $e) {
        }
        $this->loadCache();
    }

    /**
     * Store a mixed type value in cache for a certain amount of seconds.
     * Allowed values are primitives and arrays.
     *
     * @param string $key
     * @param mixed $value
     * @param int $duration Duration in seconds
     * @return mixed
     */
    public function set(string $key, $value, int $duration)
    {
        $data = [
            "t" => time(),
            "e" => $duration,
            "v" => $value,
        ];
        if ($this->cache === null) {
            $this->cache = [
                $key => $data,
            ];
        } else {
            $this->cache[$key] = $data;
        }
        $this->saveCache();
    }

    /**
     * Retrieve stored item.
     * Returns the same type as it was stored in.
     * Returns null if entry has expired.
     *
     * @param string $key
     * @return mixed|null
     */
    public function get(string $key)
    {
        if ($this->cache === null) return false;
        if (!array_key_exists($key, $this->cache)) return false;
        $data = $this->cache[$key];
        if ($this->isExpired($data)) {
            unset($this->cache[$key]);
            return false;
        }
        return true;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key): bool
    {
        if ($this->cache === null) return false;
        if (!array_key_exists($key, $this->cache)) return false;
        unset($this->cache[$key]);

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function checkCacheDir(): bool
    {
        if (!is_dir($this->dir) && !mkdir($this->dir, 0775, true)) {
            throw new Exception("Unable to create cache directory ($this->dir)");
        }
        if (!is_writable($this->dir) || !is_readable($this->dir)) {
            if (!chmod($this->dir, 0775)) {
                throw new Exception("Cache directory must be readable and writable ($this->dir)");
            }
        }
        return true;
    }

    /**
     * @return string
     */
    private function cachePath(): string
    {
        return $this->dir . md5($this->name) . $this->extension;
    }

    public function data($key): array
    {
        return $this->cache[$key];
    }

    /**
     * @return bool
     */
    private function loadCache(): bool
    {
        if (!file_exists($this->path)) return false;
        $content = file_get_contents($this->path);
        $this->cache = json_decode($content, true);
        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    private function isExpired($data): bool
    {
        if ($data["e"] == -1) return false;
        $expiresOn = $data["t"] + $data["e"];
        return $expiresOn < time();
    }

    /**
     * @return bool
     */
    private function saveCache(): bool
    {
        if ($this->cache === null) return false;
        $content = json_encode($this->cache);
        file_put_contents($this->path, $content);
        return true;
    }

}