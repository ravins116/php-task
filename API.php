<?php

class API
{
    private $uriBase;
    private $data;
    private $token;

    /**
     * API constructor.
     * @param string $data
     */
    public function __construct(string $data)
    {
        $this->uriBase = Configuration::SHIPPING_RATES;
        $this->token = base64_encode(Configuration::API_KEY);
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function response(): string
    {
        try {
            $client = new GuzzleHttp\Client();
            $response = $client->post($this->uriBase, ['headers' => ['Authorization' => 'Basic ' . $this->token], 'body' => $this->data]);
            $result = $response->getBody()->getContents();
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $result = $response->getBody()->getContents();
        }
        return $result;
    }


}