<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/Configuration.php';
require_once __DIR__ . '/Cache.php';
require_once __DIR__ . '/API.php';

$cacheKey = "ShippingRates";
$cache = new Cache();

if ($cache->get($cacheKey)) {
    $cacheData = $cache->data($cacheKey);
    echo $cacheData['v'];
} else {
    $infoData = '{
    "recipient": {
        "address1": "11025 Westlake Dr",
        "city": "Charlotte",
        "country_code": "US",
        "state_code": "NC",
        "zip": 28273
    },
     "items": [
        {
            "quantity": 2,
            "variant_id": 7679
        }
    ]
}';
    $api = new API($infoData);
    $data = $api->response();
    echo $data;
    $cache->set($cacheKey, $data, 60 * 5);
}
